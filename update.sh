#!/bin/bash
# Variáveis
VI=$(awk -F= '$1 ~ /Version/ {print $2; exit}' $HOME/tmp/teste_update_ventoy/ventoy.desktop) #Indicar o caminho
VPI=$(awk -F. '$1 ~ /Version/ {print $3; exit}' $HOME/tmp/teste_update_ventoy/ventoy.desktop) #indicar o caminho
VPP=$(echo $(($VPI+1)))
VN=1.0.$VPP
URL="https://github.com/ventoy/Ventoy/releases/download/v$VN/ventoy-$VN-linux.tar.gz"

# Números das versões
echo "Versão instalada: $VI"
echo "Versão Mais nova: $VN"
echo "Versão patch instalada: $VPI"
echo "Versão do próximo patch: $VPP"
echo $URL

# Condicional
wget -q $URL &> /dev/null
if [ $? -eq 0 ]
then
    tar -xf ventoy*.tar.gz
	sed -i "s/$VI/$VN/" ventoy.desktop #Indicar caminho de acordo comm sistema
    rm -r ventoy*.tar.gz
	cp -r ventoy-* ~/tmp #Indicar o caminho de acordo com sistema 
    rm -r ventoy-*
    rm -r ~/tmp/ventoy-$VI &> /dev/null
    echo "Aplicativo foi atualizado para versão $VN"
else
    echo "A versão $VI já é a mais atual"
fi
